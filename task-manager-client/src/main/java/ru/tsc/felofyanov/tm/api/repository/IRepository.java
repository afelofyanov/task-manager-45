package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    void clear();

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    M removeById(@NotNull String id);

    @NotNull
    M removeByIndex(@NotNull Integer index);
}


