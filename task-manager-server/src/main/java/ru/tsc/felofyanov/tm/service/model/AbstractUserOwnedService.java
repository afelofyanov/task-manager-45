package ru.tsc.felofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.api.repository.model.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.model.IUserOwnerService;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.AbstractWbs;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractWbs, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnerRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Nullable
    @Override
    public M create(@Nullable final User user, @NotNull final String name) {
        if (user == null) throw new UserNotFoundException();
        return create(user.getId(), name);
    }

    @Nullable
    @Override
    public M create(@Nullable final User user, @NotNull final String name, @NotNull final String description) {
        if (user == null) throw new UserNotFoundException();
        return create(user.getId(), name, description);
    }

    @Nullable
    @Override
    public M create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @Nullable M result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            @Nullable final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.create(userRepository.findOneById(userId), name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @Nullable M result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            @Nullable final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.create(userRepository.findOneById(userId), name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneByIdUserId(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            return repository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            return repository.findOneByIndexByUserId(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();

        @Nullable final M result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M removeByIdByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.removeByIdByUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M removeByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.removeByIndexByUserId(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public M update(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.findOneByIdUserId(userId, id);
            if (result == null) throw new ModelNotFoundException();

            result.setName(name);
            result.setDescription(description);
            repository.update(result);

            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public M updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();

            result = repository.findOneByIndexByUserId(userId, index);
            if (result == null) throw new ModelNotFoundException();
            result.setName(name);
            result.setDescription(description);
            repository.update(result);

            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();

            result = repository.findOneByIdUserId(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);

            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();

            result = repository.findOneByIndexByUserId(userId, index);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);

            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(entityManager);
            return repository.countByUserId(userId);
        } finally {
            entityManager.close();
        }
    }
}
